CREATE VIEW p_bedrijf_pct.bedrijven_view_totaal as
SELECT 
geom
, bedrijf
, bvd_id
, kvk_nummer
, vest_num
, straat
, huisnummer
, toevoeging
, postcode
, plaats
, sbi
, sbi_code
, activiteit
, sector
, hoofd1
, hoofd2
, sub
FROM p_bedrijf_pct.bedrijven_niet_in_kvk_wgs84
UNION
SELECT 
geom
, bedrijf
, bvd_id
, kvk_nummer
, vest_num
, straat
, '' as huisnummer
, '' as toevoeging
, postcode
, plaats
, sbi
, sbi_code
, activiteit
, sector
, hoofd1
, hoofd2
, sub
FROM p_bedrijf_pct.kvk_sbi;

CREATE VIEW p_bedrijf_pct.selectiecriteria_bedrijven AS
SELECT 
a.*
, CASE 
WHEN a.sbi_code = '01.30.5' THEN 1
ELSE 0
END sbi_boomkwekerij
, CASE 
WHEN a.sbi_code = '46.22' THEN 1
WHEN a.sbi_code LIKE '01.6%' THEN 1
WHEN a.sbi_code = '81.30' THEN 1
WHEN a.sbi_code = '01.19.1' THEN 1
WHEN a.sbi_code = '01.19.2' THEN 1
WHEN a.sbi_code LIKE '01.30%' THEN 1
ELSE 0
END sbi_gerelateerd
, CASE 
WHEN b.geom IS NOT NULL THEN 1
ELSE 0
END op_boomkwekerij_grond
, CASE 
WHEN c.geom IS NOT NULL THEN 1
ELSE 0
END op_boomkwekerij_perceel
, CASE 
WHEN d.geom IS NOT NULL THEN 1
ELSE 0
END naast_boomkwekerij_perceel
, CASE 
WHEN e.geom IS NOT NULL THEN 1
ELSE 0
END binnen_sierteeltgebied
FROM p_bedrijf_pct.bedrijven_view_totaal a
LEFT JOIN p_bedrijf_pct.boomkwekerijen_grond b
ON ST_Intersects(a.geom,b.geom)
LEFT JOIN p_bedrijf_pct.percelen_met_boomkwekerij c
ON ST_Intersects(a.geom,c.geom)
LEFT JOIN p_bedrijf_pct.percelen_aangrenzend_aan_boomkwekerij d
ON ST_Intersects(a.geom,d.geom)
LEFT JOIN p_bedrijf_pct.sierteeltgebieden e
ON ST_Intersects(a.geom,e.geom)
ORDER BY binnen_sierteeltgebied DESC 
;

CREATE VIEW p_bedrijf_pct.selectiecriteria_score_bedrijven AS
SELECT *
, op_boomkwekerij_grond + op_boomkwekerij_perceel + binnen_sierteeltgebied as score_ligging
FROM p_bedrijf_pct.selectiecriteria_bedrijven
;

CREATE VIEW p_bedrijf_pct.selectie_bedrijven AS
 SELECT selectiecriteria_score_bedrijven.geom,
    selectiecriteria_score_bedrijven.bvd_id,
    selectiecriteria_score_bedrijven.kvk_nummer,
    selectiecriteria_score_bedrijven.vest_num,
    selectiecriteria_score_bedrijven.straat,
    selectiecriteria_score_bedrijven.huisnummer,
    selectiecriteria_score_bedrijven.toevoeging,
    selectiecriteria_score_bedrijven.postcode,
    selectiecriteria_score_bedrijven.plaats,
    selectiecriteria_score_bedrijven.sbi,
    selectiecriteria_score_bedrijven.sbi_code,
    selectiecriteria_score_bedrijven.activiteit,
    selectiecriteria_score_bedrijven.sector,
    selectiecriteria_score_bedrijven.hoofd1,
    selectiecriteria_score_bedrijven.hoofd2,
    selectiecriteria_score_bedrijven.sub,
    selectiecriteria_score_bedrijven.sbi_boomkwekerij,
    selectiecriteria_score_bedrijven.sbi_gerelateerd,
    selectiecriteria_score_bedrijven.op_boomkwekerij_grond,
    selectiecriteria_score_bedrijven.op_boomkwekerij_perceel,
    selectiecriteria_score_bedrijven.naast_boomkwekerij_perceel,
	selectiecriteria_score_bedrijven.binnen_sierteeltgebied,
    selectiecriteria_score_bedrijven.score_ligging
   FROM p_bedrijf_pct.selectiecriteria_score_bedrijven
  WHERE selectiecriteria_score_bedrijven.sbi_boomkwekerij > 0 OR selectiecriteria_score_bedrijven.sbi_gerelateerd > 0 OR selectiecriteria_score_bedrijven.score_ligging > 0
  GROUP BY
  selectiecriteria_score_bedrijven.geom,
    selectiecriteria_score_bedrijven.bvd_id,
    selectiecriteria_score_bedrijven.kvk_nummer,
    selectiecriteria_score_bedrijven.vest_num,
    selectiecriteria_score_bedrijven.straat,
    selectiecriteria_score_bedrijven.huisnummer,
    selectiecriteria_score_bedrijven.toevoeging,
    selectiecriteria_score_bedrijven.postcode,
    selectiecriteria_score_bedrijven.plaats,
    selectiecriteria_score_bedrijven.sbi,
    selectiecriteria_score_bedrijven.sbi_code,
    selectiecriteria_score_bedrijven.activiteit,
    selectiecriteria_score_bedrijven.sector,
    selectiecriteria_score_bedrijven.hoofd1,
    selectiecriteria_score_bedrijven.hoofd2,
    selectiecriteria_score_bedrijven.sub,
    selectiecriteria_score_bedrijven.sbi_boomkwekerij,
    selectiecriteria_score_bedrijven.sbi_gerelateerd,
    selectiecriteria_score_bedrijven.op_boomkwekerij_grond,
    selectiecriteria_score_bedrijven.op_boomkwekerij_perceel,
    selectiecriteria_score_bedrijven.naast_boomkwekerij_perceel,
	selectiecriteria_score_bedrijven.binnen_sierteeltgebied,
    selectiecriteria_score_bedrijven.score_ligging
	;
    
CREATE VIEW p_bedrijf_pct.selectie_bedrijven_deelgebieden AS
SELECT
a.*
, b.deelgebied
FROM p_bedrijf_pct.selectie_bedrijven a
LEFT JOIN p_bedrijf_pct.deelgebieden b
ON ST_Intersects(a.geom,b.geom)
;